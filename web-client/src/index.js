import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Dashboard from "./views/Dashboard";
import Icons from "./views/Icons";
import Maps from "./views/Maps";
import Notifications from "./views/Notifications";
import TableList from "./views/TableList";

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/animate.min.css";
// import "./assets/scss/light-bootstrap-dashboard-react.scss?v=2.0.0";
import "./assets/css/demo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";


ReactDOM.render(
  <BrowserRouter>
  <Switch>
      <Route path="/Dashboard" render={() => <Dashboard />} />
      <Route path="/Icons" render={() => <Icons />} />
      <Route path="/Maps" render={() => <Maps />} />
      <Route path="/Notifications" render={() => <Notifications />} />
      <Route path="/TableList" render={() => <TableList />} />

      <Redirect to="/Dashboard" />
   </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
