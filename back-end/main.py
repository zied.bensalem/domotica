from flask import Flask, escape, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, Float, String
import os
import json



app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'planets.db')
db = SQLAlchemy(app)


@app.cli.command('db_create')
def db_create():
    db.create_all()
    print('Database created !')


@app.cli.command('db_drop')
def db_drop():
    db.drop_all()
    print ('Data base dropped !')


@app.cli.command('db_seed')
def db_seed():
    mercury = Planet(planet_name='Mercury',
                     planet_type='Class D',
                     home_star='Sol',
                     mass=3.258e23,
                     radius=1516,
                     distance=35.98e6)
    
    venus = Planet(planet_name='Venus',
                   planet_type='Class K',
                   home_star='Sol',
                   mass=8.258e23,
                   radius=1716,
                   distance=65.98e6)

    earth = Planet(planet_name='Earth',
                   planet_type='Class M',
                   home_star='Sol',
                   mass=5.158e23,
                   radius=1616,
                   distance=92.98e6)

    db.session.add(mercury)
    db.session.add(venus)
    db.session.add(earth)

    test_user = User(first_name='Zied',
                    last_name='BEN SALEM',
                    email='z@z.com',
                    password='azerty123')
    db.session.add(test_user)
    db.session.commit()
    print('Data base seeded')

@app.route('/')
def hello():
    name = request.args.get("name", "World")
    mymessage = "hello Z"+name
    return jsonify(bla=mymessage), 200


def read_json(file):
    with open(file) as json_file:
        data = json.load(json_file)
    return data


@app.route('/json')    
def test_json():
    f=read_json("test.json")
    return f


@app.route('/not_found')
def not_found():
    return jsonify(message='That ressource was not found'), 404


@app.route('/parameters')
def parameters():
    name = request.args.get('name')
    age = int(request.args.get('age'))
    if age < 18:
        return jsonify(message="Sorry,"+ name + " you are not old enough"), 401
    else:
        return jsonify(message="Welcome "+ name + ", you are old enough!"), 200


@app.route('/url_var/<string:name>/<int:age>')
def url_var (name:str, age:int):
    if age < 18:
        return jsonify(message="Sorry,"+ name + " you are NOT old enough"), 401
    else:
        return jsonify(message="Welcome "+ name + ", you are old enough!"), 200


# Data Models
class User(db.Model):
    __tablename__ = 'users'
    id = Column (Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column (String)
    email = Column (String, unique=True)
    password = Column (String)

class Planet (db.Model):
    __tablename__ ='planets'
    planet_id = Column(Integer, primary_key=True)
    planet_name = Column(String)
    planet_type = Column(String)
    home_star = Column(String)
    mass=Column(Float)
    radius=Column(Float)
    distance=Column(Float)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0",port="5000")