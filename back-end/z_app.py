from flask import Flask, escape, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, Float, String
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager, jwt_required, create_access_token
from flask_mail import Mail, Message
import os
import json


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'planets.db')
app.config['JWT_SECRET_KEY']='super-secret' #to be changed in real life
app.config['MAIL_SERVER']='smtp.mailtrap.io'
#app.config['MAIL_USERNAME']=os.environ['MAIL_USERNAME']
#app.config['MAIL_PASSWORD']=os.environ['MAIL_PASSWORD']
app.config['MAIL_SERVER']='smtp.mailtrap.io'
app.config['MAIL_PORT'] = 2525
app.config['MAIL_USERNAME'] = '8f5baac0a7a4c6'
app.config['MAIL_PASSWORD'] = 'b663f69aee2397'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)
jwt = JWTManager(app)
mail = Mail(app)

@app.cli.command('db_create')
def db_create():
    db.create_all()
    print('Database created !')


@app.cli.command('db_drop')
def db_drop():
    db.drop_all()
    print ('Data base dropped !')


@app.cli.command('db_seed')
def db_seed():
    mercury = Planet(planet_name='Mercury',
                     planet_type='Class D',
                     home_star='Sol',
                     mass=3.258e23,
                     radius=1516,
                     distance=35.98e6)
    
    venus = Planet(planet_name='Venus',
                   planet_type='Class K',
                   home_star='Sol',
                   mass=8.258e23,
                   radius=1716,
                   distance=65.98e6)

    earth = Planet(planet_name='Earth',
                   planet_type='Class M',
                   home_star='Sol',
                   mass=5.158e23,
                   radius=1616,
                   distance=92.98e6)

    db.session.add(mercury)
    db.session.add(venus)
    db.session.add(earth)

    test_user = User(first_name='Zied',
                    last_name='BEN SALEM',
                    email='z@z.com',
                    password='azerty123')
    db.session.add(test_user)
    db.session.commit()
    print('Data base seeded')


@app.route('/json')    
def test_json():
    f=read_json("test.json")
    return f


@app.route('/not_found')
def not_found():
    return jsonify(message='That ressource was not found'), 404


@app.route('/parameters')
def parameters():
    name = request.args.get('name')
    age = int(request.args.get('age'))
    if age < 18:
        return jsonify(message="Sorry,"+ name + " you are not old enough"), 401
    else:
        return jsonify(message="Welcome "+ name + ", you are old enough!"), 200


@app.route('/url_var/<string:name>/<int:age>')
def url_var (name:str, age:int):
    if age < 18:
        return jsonify(message="Sorry,"+ name + " you are NOT old enough"), 401
    else:
        return jsonify(message="Welcome "+ name + ", you are old enough!"), 200

@app.route('/planets',methods=['GET'])
def planets():
    planets_list = Planet.query.all()
    result = planets_schema.dump(planets_list)
    return jsonify(result)

@app.route('/register',methods=['POST'])
def register():
    email=request.form['email']
    test= User.query.filter_by(email=email).first()
    if test:
        return jsonify(message='That email already exist'), 409
    else:
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        password = request.form['password']
        user=User(first_name=first_name, last_name=last_name,email=email,password=password)
        db.session.add(user)
        db.session.commit()
        return jsonify(message='User created sucessfully'), 201

@app.route('/login', methods=['POST'])
def login():
    if request.is_json:
        email=request.json['email']
        password=request.json['password']
        test=User.query.filter_by(email=email,password=password).first()

    else:
        email =request.form['email']
        password=request.form['password']
    
    if test:
        access_token=create_access_token(identity=email)
        return jsonify(message="Login succeeded!",access_token=access_token)
    else:
        return jsonify(message="Bad email or password"), 401

@app.route('/retreive_password/<string:email>', methods=['GET'])
def restreive_email(email:str):
    user=User.query.filter_by(email=email).first()
    print("email to be retrieved is :", email)
    if user:
        print("User is verified, passwordrecovery email will be sent")
        msg= Message("Your Planetary API password is: "+ user.password, sender="admin@planetary-api.com", recipients=[email])
        mail.send(msg)
        return jsonify (message="Password sent to "+ email)
    else:
        return jsonify (message="That email does not exist"), 401

# Data Models
class User(db.Model):
    __tablename__ = 'users'
    id = Column (Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column (String)
    email = Column (String, unique=True)
    password = Column (String)

class Planet (db.Model):
    __tablename__ ='planets'
    planet_id = Column(Integer, primary_key=True)
    planet_name = Column(String)
    planet_type = Column(String)
    home_star = Column(String)
    mass=Column(Float)
    radius=Column(Float)
    distance=Column(Float)

class UserSchema(ma.Schema):
    class Meta:
        fields=('id','first_name','last_name','email','password')

class PlanetSchema(ma.Schema):
    class Meta:
        fields=('planet_id','planet_name', 'planet_type', 'home_star','mass','radius','distance')

user_schema = UserSchema()
users_schema = UserSchema(many=True)

planet_schema = PlanetSchema()
planets_schema = PlanetSchema(many=True)

if __name__ == "__main__":
    app.run(debug=True, host="localhost",port="5000")